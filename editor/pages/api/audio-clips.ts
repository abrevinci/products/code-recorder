import type { NextApiRequest, NextApiResponse } from 'next';
import middleware from '../../middleware/middleware';
import nextConnect from 'next-connect';
import { randomUUID } from 'crypto';
import fs from 'fs';

type UploadRequest = NextApiRequest & { files: any };

const handler = nextConnect();
handler.use(middleware);

handler.delete(async (req: NextApiRequest, res: NextApiResponse) => {
  const url = req.body.url;
  //const fileName = url.split('/').pop();
  //   const dir = `./public/uploads/audio-clips/trash`;
  //   if (!fs.existsSync(dir)) {
  //     fs.mkdirSync(dir, { recursive: true });
  //   }

  const oldPath = `./public${url}`;
  //const newPath = `./public/uploads/audio-clips/trash/${fileName}`;
  fs.rmSync(oldPath);
  res.status(200).end(); //.send(`uploads/audio-clips/trash/${fileName}`);
});

handler.post(async (req: UploadRequest, res: NextApiResponse) => {
  const date = new Date().toISOString().slice(0, 10);
  const url = `/uploads/audio-clips/${date}-${randomUUID()}.wav`;
  const dir = `./public/uploads/audio-clips`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }

  const oldPath = req.files.audioclip[0].path;
  const newPath = `./public${url}`;
  fs.renameSync(oldPath, newPath);

  res.status(200).send(url);
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
