import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import fs from 'fs';

const handler = nextConnect();

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  if (fs.existsSync('./data/project.json')) {
    const json = fs.readFileSync('./data/project.json');
    res.status(200).send(JSON.parse(json as any));
  } else {
    res.status(404).end();
  }
});

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const json = JSON.stringify(req.body, null, 2);
  if (!fs.existsSync('./data')) {
    fs.mkdirSync('./data', { recursive: true });
  }
  fs.writeFileSync('./data/project.json', json);
  res.status(200).end();
});

export default handler;
