import dynamic from 'next/dynamic';
import type { NextPage } from 'next';
import { useEffect } from 'react';
import { useAppDispatch } from '../core/hooks';
import { loadProject, ProjectState, saveProject } from '../core/projectSlice';

const ProjectEditor = dynamic(() => import('../components/ProjectEditor'), {
  ssr: false,
});

const Home: NextPage = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadProject());
  }, []);

  return <ProjectEditor />;
};

export default Home;
