import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

export type Video = {
  id: number;
  name: string;
  sourceFileIds: number[];
  revisionIds: number[];
  expandedRevisionIds: number[];
};

export type SourceFile = {
  id: number;
  path: string;
  revisionIds: number[];
};

export type Revision = {
  id: number;
  sourceFileId: number;
  code: string;
  useTypeWriter: boolean;
  highlightChanges: boolean;
  animationRange: [number, number];
  fileAdded: boolean;
  fileRemoved: boolean;
  transcript: string;
  audioClipIds: number[];
};

export type AudioClip = {
  id: number;
  url: string;
  start: number;
  end: number;
};

export type ProjectState = {
  videos: {
    [id: number]: Video;
  };
  videoIds: number[];
  sourceFiles: {
    [id: number]: SourceFile;
  };
  revisions: {
    [id: number]: Revision;
  };
  audioClips: {
    [id: number]: AudioClip;
  };
  currentVideoId?: number;
  currentRevisionId?: number;
  isRecordingAudio: boolean;
  revisionEditorTab: string;
};

const initialState: ProjectState = {
  videos: {},
  videoIds: [],
  sourceFiles: {},
  revisions: {},
  audioClips: {},
  isRecordingAudio: false,
  revisionEditorTab: '1',
};

const generateId = (collection: { [id: number]: any }) => {
  let id = 1;
  while (collection[id]) {
    id += 1;
  }
  return id;
};

export const onAudioClipRecorded = createAsyncThunk<{ url: string; duration: number }, { blob: Blob; duration: number }>('project/onAudioClipRecorded', async payload => {
  const formData = new FormData();
  formData.append('audioclip', payload.blob);
  const response = await fetch('/api/audio-clips', { method: 'POST', body: formData });
  const url = await response.text();
  return { url, duration: payload.duration };
});

export const onAudioClipUpdated = createAsyncThunk<{ id: number; url: string; duration: number }, { id: number; url: string; blob: Blob; duration: number }>(
  'project/onAudioClipUpdated',
  async payload => {
    let formData = new FormData();
    formData.append('audioclip', payload.blob);
    const url = await fetch('/api/audio-clips', { method: 'POST', body: formData });
    formData = new FormData();
    formData.append('url', payload.url);
    await fetch('/api/audio-clips', { method: 'DELETE', body: formData });
    return { id: payload.id, url: await url.text(), duration: payload.duration };
  }
);

export const removeAudioClip = createAsyncThunk<number, number, { state: { project: ProjectState } }>('project/removeAudioClip', async (id, thunkAPI) => {
  const state = thunkAPI.getState().project;
  const clip = state.audioClips[id];
  const formData = new FormData();
  formData.append('url', clip.url);
  await fetch('/api/audio-clips', { method: 'DELETE', body: formData });
  return id;
});

export const loadProject = createAsyncThunk<ProjectState, void>('project/loadProject', async () => {
  console.log('loading...');
  const response = await fetch('/api/project', { method: 'GET' });
  if (response.status === 200) {
    console.log('project found');
    const proj = await response.json();
    console.log(proj);
    return proj;
  } else {
    console.log('no project found');
    return project;
  }
});

export const saveProject = createAsyncThunk<void, void, { state: { project: ProjectState } }>('project/saveProject', async (_, thunkAPI) => {
  await fetch('/api/project', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(thunkAPI.getState().project),
  });
});

export const projectSlice = createSlice({
  name: 'state',
  initialState,
  reducers: {
    addNewVideo: (state: ProjectState, action: PayloadAction<string>) => {
      const id = generateId(state.videos);
      state.videos[id] = { id, name: action.payload, sourceFileIds: [], revisionIds: [], expandedRevisionIds: [] };
      state.videoIds.push(id);
      state.currentVideoId = id;
    },
    addFollowUpVideo: (state: ProjectState, action: PayloadAction<string>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      const currentVideo = state.videos[state.currentVideoId];
      const id = generateId(state.videos);
      state.videos[id] = {
        id,
        name: action.payload,
        sourceFileIds: currentVideo.sourceFileIds,
        revisionIds: currentVideo.sourceFileIds
          .map(fid => state.sourceFiles[fid])
          .filter(f => f.revisionIds.length > 0)
          .map(f => f.revisionIds[f.revisionIds.length - 1]),
        expandedRevisionIds: [],
      };
      state.videoIds.push(id);
      state.currentVideoId = id;
      const revisionIds = state.videos[state.currentVideoId].revisionIds;
      if (revisionIds.length > 0) {
        const revision = state.revisions[revisionIds[revisionIds.length - 1]];
        state.currentRevisionId = revision.id;
      } else {
        delete state.currentRevisionId;
      }
    },
    setCurrentVideoId: (state: ProjectState, action: PayloadAction<number>) => {
      state.currentVideoId = action.payload;
      const revisionIds = state.videos[state.currentVideoId].revisionIds;
      if (revisionIds.length > 0) {
        const revision = state.revisions[revisionIds[revisionIds.length - 1]];
        state.currentRevisionId = revision.id;
      } else {
        delete state.currentRevisionId;
      }
    },
    addSourceFile: (state: ProjectState, action: PayloadAction<string>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      const currentVideo = state.videos[state.currentVideoId];
      const id = generateId(state.sourceFiles);
      const revisionId = generateId(state.revisions);
      state.sourceFiles[id] = { id, path: action.payload, revisionIds: [revisionId] };
      state.revisions[revisionId] = {
        id: revisionId,
        sourceFileId: id,
        code: '',
        useTypeWriter: false,
        highlightChanges: false,
        animationRange: [0, 0],
        fileAdded: true,
        fileRemoved: false,
        transcript: '',
        audioClipIds: [],
      };
      state.currentRevisionId = revisionId;
      currentVideo.sourceFileIds.push(id);
      currentVideo.revisionIds.push(revisionId);
      currentVideo.expandedRevisionIds.push(revisionId);
    },
    addRevision: (state: ProjectState, action: PayloadAction<number>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      const afterId = action.payload;
      const afterRevision = state.revisions[afterId];
      const currentVideo = state.videos[state.currentVideoId];
      const sourceFile = state.sourceFiles[afterRevision.sourceFileId];
      const id = generateId(state.revisions);
      state.revisions[id] = {
        id,
        sourceFileId: afterRevision.sourceFileId,
        code: state.currentRevisionId !== undefined ? state.revisions[state.currentRevisionId].code : '',
        useTypeWriter: false,
        highlightChanges: true,
        animationRange: [0, 0],
        fileAdded: false,
        fileRemoved: false,
        transcript: '',
        audioClipIds: [],
      };
      state.currentRevisionId = id;
      const afterIdVideoIndex = currentVideo.revisionIds.indexOf(afterId);
      if (afterIdVideoIndex >= 0 && afterIdVideoIndex < currentVideo.revisionIds.length - 1) {
        currentVideo.revisionIds.splice(afterIdVideoIndex + 1, 0, id);
      } else {
        currentVideo.revisionIds.push(id);
      }
      const afterIdFileIndex = sourceFile.revisionIds.indexOf(afterId);
      if (afterIdFileIndex >= 0 && afterIdFileIndex < sourceFile.revisionIds.length - 1) {
        sourceFile.revisionIds.splice(afterIdFileIndex + 1, 0, id);
      } else {
        sourceFile.revisionIds.push(id);
      }
      if (currentVideo.expandedRevisionIds.includes(afterId)) {
        currentVideo.expandedRevisionIds.push(id);
      }
    },
    removeRevision: (state: ProjectState, action: PayloadAction<number>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      const currentVideo = state.videos[state.currentVideoId];
      currentVideo.revisionIds = currentVideo.revisionIds.filter(id => id !== action.payload);
      if (!Object.values(state.videos).some(video => video.revisionIds.includes(action.payload))) {
        delete state.revisions[action.payload];
        for (const file of Object.values(state.sourceFiles)) {
          file.revisionIds = file.revisionIds.filter(id => id !== action.payload);
        }
      }
      if (state.currentRevisionId === action.payload) {
        if (currentVideo.revisionIds.length > 0) {
          state.currentRevisionId = currentVideo.revisionIds[currentVideo.revisionIds.length - 1];
        } else {
          delete state.currentRevisionId;
        }
      }
      currentVideo.expandedRevisionIds = currentVideo.expandedRevisionIds.filter(id => id !== action.payload);
    },
    setCurrentRevisionId: (state: ProjectState, action: PayloadAction<number>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      state.currentRevisionId = action.payload;
    },
    setCode: (state: ProjectState, action: PayloadAction<string>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.revisions[state.currentRevisionId].code = action.payload;
    },
    setUseTypeWriter: (state: ProjectState, action: PayloadAction<boolean>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.revisions[state.currentRevisionId].useTypeWriter = action.payload;
    },
    setHighlightChanges: (state: ProjectState, action: PayloadAction<boolean>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.revisions[state.currentRevisionId].highlightChanges = action.payload;
    },
    setAnimationRange: (state: ProjectState, action: PayloadAction<[number, number]>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.revisions[state.currentRevisionId].animationRange = action.payload;
    },
    swapRevisions: (state: ProjectState, action: PayloadAction<{ firstId: number; secondId: number }>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      const currentVideo = state.videos[state.currentVideoId];
      const firstRevision = state.revisions[action.payload.firstId];
      const secondRevision = state.revisions[action.payload.secondId];
      const firstRevisionVideoIndex = currentVideo.revisionIds.indexOf(firstRevision.id);
      const secondRevisionVideoIndex = currentVideo.revisionIds.indexOf(secondRevision.id);
      currentVideo.revisionIds[firstRevisionVideoIndex] = secondRevision.id;
      currentVideo.revisionIds[secondRevisionVideoIndex] = firstRevision.id;
      if (firstRevision.sourceFileId == secondRevision.sourceFileId) {
        const file = state.sourceFiles[firstRevision.sourceFileId];
        const firstRevisionFileIndex = file.revisionIds.indexOf(firstRevision.id);
        const secondRevisionFileIndex = file.revisionIds.indexOf(secondRevision.id);
        file.revisionIds[firstRevisionFileIndex] = secondRevision.id;
        file.revisionIds[secondRevisionFileIndex] = firstRevision.id;
      }
      const firstIsExpanded = currentVideo.expandedRevisionIds.includes(firstRevision.id);
      const secondIsExpanded = currentVideo.expandedRevisionIds.includes(secondRevision.id);
      let i = secondRevisionVideoIndex + 1;
      while (i < currentVideo.revisionIds.length) {
        const revision = state.revisions[currentVideo.revisionIds[i]];
        if (revision.sourceFileId === firstRevision.sourceFileId) {
          if (firstIsExpanded) {
            currentVideo.expandedRevisionIds = Array.from(new Set<number>([...currentVideo.expandedRevisionIds, revision.id]));
          } else if (currentVideo.expandedRevisionIds.includes(revision.id)) {
            currentVideo.expandedRevisionIds = Array.from(new Set<number>([...currentVideo.expandedRevisionIds, firstRevision.id]));
          }
        }
        i += 1;
      }
      i = firstRevisionVideoIndex - 1;
      while (i >= 0) {
        const revision = state.revisions[currentVideo.revisionIds[i]];
        if (revision.sourceFileId === secondRevision.sourceFileId) {
          if (secondIsExpanded) {
            currentVideo.expandedRevisionIds = Array.from(new Set<number>([...currentVideo.expandedRevisionIds, revision.id]));
          } else if (currentVideo.expandedRevisionIds.includes(revision.id)) {
            currentVideo.expandedRevisionIds = Array.from(new Set<number>([...currentVideo.expandedRevisionIds, secondRevision.id]));
          }
        }
        i -= 1;
      }
    },
    onAudioClipRecording: (state: ProjectState) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.isRecordingAudio = true;
    },
    setAudioClipRange: (state: ProjectState, action: PayloadAction<{ id: number; start: number; end: number }>) => {
      state.audioClips[action.payload.id].start = action.payload.start;
      state.audioClips[action.payload.id].end = action.payload.end;
    },
    swapAudioClips: (state: ProjectState, action: PayloadAction<{ firstId: number; secondId: number }>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      const currentRevision = state.revisions[state.currentRevisionId];
      const firstIdIndex = currentRevision.audioClipIds.indexOf(action.payload.firstId);
      const secondIdIndex = currentRevision.audioClipIds.indexOf(action.payload.secondId);
      currentRevision.audioClipIds[firstIdIndex] = action.payload.secondId;
      currentRevision.audioClipIds[secondIdIndex] = action.payload.firstId;
    },
    setTranscript: (state: ProjectState, action: PayloadAction<string>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.revisions[state.currentRevisionId].transcript = action.payload;
    },
    setRevisionsExpanded: (state: ProjectState, action: PayloadAction<{ expanded: boolean; ids: number[] }>) => {
      if (state.currentVideoId === undefined) {
        return;
      }
      const currentVideo = state.videos[state.currentVideoId];
      if (action.payload.expanded) {
        for (const id of action.payload.ids) {
          if (!currentVideo.expandedRevisionIds.includes(id)) {
            currentVideo.expandedRevisionIds.push(id);
          }
        }
      } else {
        currentVideo.expandedRevisionIds = currentVideo.expandedRevisionIds.filter(id => !action.payload.ids.includes(id));
      }
    },
    setRevisionEditorTab: (state: ProjectState, action: PayloadAction<string>) => {
      state.revisionEditorTab = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(onAudioClipRecorded.fulfilled, (state: ProjectState, action: PayloadAction<{ url: string; duration: number }>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      const id = generateId(state.audioClips);
      state.audioClips[id] = {
        id,
        url: action.payload.url,
        start: 0,
        end: action.payload.duration,
      };
      console.log(state.audioClips[id]);
      state.revisions[state.currentRevisionId].audioClipIds.push(id);
      state.isRecordingAudio = false;
    });
    builder.addCase(onAudioClipUpdated.fulfilled, (state: ProjectState, action: PayloadAction<{ id: number; url: string; duration: number }>) => {
      const clip = state.audioClips[action.payload.id];
      clip.url = action.payload.url;
      clip.start = 0;
      clip.end = action.payload.duration;
    });
    builder.addCase(removeAudioClip.fulfilled, (state: ProjectState, action: PayloadAction<number>) => {
      if (state.currentRevisionId === undefined) {
        return;
      }
      state.revisions[state.currentRevisionId].audioClipIds = state.revisions[state.currentRevisionId].audioClipIds.filter(id => id !== action.payload);
      delete state.audioClips[action.payload];
    });
    builder.addCase(loadProject.fulfilled, (state: ProjectState, action: PayloadAction<ProjectState>) => {
      state.videos = action.payload.videos;
      state.videoIds = action.payload.videoIds;
      state.sourceFiles = action.payload.sourceFiles;
      state.revisions = action.payload.revisions;
      state.audioClips = action.payload.audioClips;
      if (action.payload.currentVideoId !== undefined) {
        state.currentVideoId = action.payload.currentVideoId;
      } else {
        delete state.currentVideoId;
      }
      if (action.payload.currentRevisionId !== undefined) {
        state.currentRevisionId = action.payload.currentRevisionId;
      } else {
        delete state.currentRevisionId;
      }
      state.revisionEditorTab = action.payload.revisionEditorTab;
    });
  },
});

export const {
  addNewVideo,
  addFollowUpVideo,
  setCurrentVideoId,
  addSourceFile,
  addRevision,
  removeRevision,
  setCurrentRevisionId,
  setCode,
  setUseTypeWriter,
  setHighlightChanges,
  setAnimationRange,
  swapRevisions,
  onAudioClipRecording,
  setAudioClipRange,
  swapAudioClips,
  setTranscript,
  setRevisionsExpanded,
  setRevisionEditorTab,
} = projectSlice.actions;

export default projectSlice.reducer;

export const getCurrentRevision = (state: ProjectState) => {
  return state.currentRevisionId ? state.revisions[state.currentRevisionId] : undefined;
};

export const getCurrentVideo = (state: ProjectState) => {
  return state.currentVideoId ? state.videos[state.currentVideoId] : undefined;
};

export const getPreviousRevision = (state: ProjectState) => {
  const currentVideo = getCurrentVideo(state);
  if (currentVideo && state.currentRevisionId) {
    const index = currentVideo.revisionIds.indexOf(state.currentRevisionId);
    if (index > 0) {
      return state.revisions[currentVideo.revisionIds[index - 1]];
    }
  }
  return undefined;
};

export const getNextRevision = (state: ProjectState) => {
  const currentVideo = getCurrentVideo(state);
  if (currentVideo && state.currentRevisionId) {
    const index = currentVideo.revisionIds.indexOf(state.currentRevisionId);
    if (index < currentVideo.revisionIds.length - 1) {
      return state.revisions[currentVideo.revisionIds[index + 1]];
    }
  }
  return undefined;
};

const project: ProjectState = {
  videos: {
    [1]: {
      id: 1,
      name: 'Simple Counter',
      sourceFileIds: [1, 2],
      revisionIds: [1, 2, 3, 4],
      expandedRevisionIds: [],
    },
  },
  videoIds: [1],
  sourceFiles: {
    [1]: {
      id: 1,
      path: 'MyApp/Core/Message.cs',
      revisionIds: [1, 2, 3],
    },
    [2]: {
      id: 2,
      path: 'MyApp/Core/Actions.cs',
      revisionIds: [4],
    },
  },
  revisions: {
    [1]: {
      id: 1,
      sourceFileId: 1,
      code: `
namespace MyApp.Core;

public abstract record Message
{
\t// prevent external inheritance
\tprivate Message() {}
}
      `.trim(),
      useTypeWriter: false,
      highlightChanges: false,
      animationRange: [0, 0],
      fileAdded: false,
      fileRemoved: false,
      transcript:
        'Here we are inside the message file. The message type itself is an abstract record which means that we will be inheriting from it. And we will do so in here so that all our messages are defined in one place. Next we want to add a message here.',
      audioClipIds: [],
    },
    [2]: {
      id: 2,
      sourceFileId: 1,
      code: `
namespace MyApp.Core;

public abstract record Message
{
\tpublic sealed record Increment

\t// prevent external inheritance
\tprivate Message() {}
}
      `.trim(),
      useTypeWriter: false,
      highlightChanges: false,
      animationRange: [0, 0],
      fileAdded: false,
      fileRemoved: false,
      transcript: 'We do that by adding another record in here that we call increment and we make it sealed.',
      audioClipIds: [],
    },
    [3]: {
      id: 3,
      sourceFileId: 1,
      code: `
namespace MyApp.Core;

public abstract record Message
{
\tpublic sealed record Increment : Message;

\t// prevent external inheritance
\tprivate Message() {}
}
      `.trim(),
      useTypeWriter: false,
      highlightChanges: false,
      animationRange: [0, 0],
      fileAdded: false,
      fileRemoved: false,
      transcript:
        'Then we make sure to inherit from Message. If you are familiar with functional languages you might recognize that we are trying to mimic a discriminated union here as that feature is unfortunately not yet a part of the C# language. Here we are basically creating a closed class hierarchy instead using records for brevity and immutability.',
      audioClipIds: [],
    },
    [4]: {
      id: 4,
      sourceFileId: 2,
      code: `
namespace MyApp.Core;

public static class Actions
{
}
      `.trim(),
      useTypeWriter: false,
      highlightChanges: false,
      animationRange: [0, 0],
      fileAdded: false,
      fileRemoved: false,
      transcript: 'Here we are inside the actions file. There are no actions in here yet.',
      audioClipIds: [],
    },
  },
  audioClips: {},
  currentVideoId: 1,
  isRecordingAudio: false,
  revisionEditorTab: '1',
};
