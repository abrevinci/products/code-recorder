import Prism from 'prismjs';
import { FunctionComponent, useEffect, useRef } from 'react';

type CodeEditorProps = {
  preview?: boolean;
  filePath: string;
  code: string;
  setCode: (code: string) => void;
  createRevision: () => void;
};

const CodeEditor: FunctionComponent<CodeEditorProps> = ({ preview = false, filePath, code, setCode, createRevision }) => {
  const codeElement = useRef(null);
  const textAreaElement = useRef<HTMLTextAreaElement | null>(null);
  const preElement = useRef<HTMLPreElement>(null);

  const highlight = () => {
    if (codeElement.current) {
      Prism.highlightElement(codeElement.current);
    }
  };

  const onScroll = () => {
    if (preElement.current && textAreaElement.current) {
      preElement.current.scrollTop = textAreaElement.current.scrollTop;
      preElement.current.scrollLeft = textAreaElement.current.scrollLeft;
    }
  };

  const onKeyDown = (e: any) => {
    if (!textAreaElement.current) {
      return;
    }
    const element = textAreaElement.current;
    const code = element.value;
    if (e.key == 'Tab') {
      e.preventDefault();
      let before_tab = code.slice(0, element.selectionStart);
      let after_tab = code.slice(element.selectionEnd, element.value.length);
      let cursor_pos = element.selectionEnd + 1;
      element.value = before_tab + '\t' + after_tab;
      element.selectionStart = cursor_pos;
      element.selectionEnd = cursor_pos;
      setCode(element.value);
    }
  };

  useEffect(() => {
    const onHotkey = (e: KeyboardEvent) => {
      if (e.ctrlKey && e.key === 'q') {
        createRevision();
      }
    };

    document.addEventListener('keydown', onHotkey);
    return () => {
      document.removeEventListener('keydown', onHotkey);
    };
  }, [createRevision]);

  useEffect(() => {
    highlight();
  });

  const language = filePath.endsWith('.xaml') || filePath.endsWith('.csproj') ? 'xml' : 'csharp';

  let codeToShow = code
    .replace(/<highlight>/g, preview ? '___HIGHLIGHT_BEGIN___' : '')
    .replace(/<\/highlight>/g, preview ? '___HIGHLIGHT_END___' : '')
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/___HIGHLIGHT_END___/g, '</highlight>')
    .replace(/___HIGHLIGHT_BEGIN___/g, '<highlight>');
  if (preview) {
    codeToShow = codeToShow.replace(/\/\*___SQUIGGLY_BEGIN___\*\//g, '<squiggly>').replace(/\/\*___SQUIGGLY_END___\*\//g, '</squiggly>');
  }
  const displayCode = codeToShow.length === 0 || codeToShow[codeToShow.length - 1] === '\n' ? codeToShow + ' ' : codeToShow;

  return (
    <>
      <div className="code-editor">
        <pre className={`language-${language}`} style={{ paddingLeft: '3.8em', opacity: 0.7, borderBottom: '1px solid black', marginTop: 0, marginBottom: 0 }}>
          <code className={`language-${language}`}>
            <span className="token variable">{filePath}</span>
          </code>
        </pre>
        <div className="code-edit-container">
          {!preview && (
            <textarea
              ref={textAreaElement}
              className="code-edit-input styled-scrollbars"
              value={code}
              onChange={e => setCode(e.target.value)}
              onScroll={onScroll}
              onKeyDown={onKeyDown}
              spellCheck={false}
            />
          )}
          <pre ref={preElement} className={`code-edit-output language-${language} styled-scrollbars line-numbers`}>
            <code ref={codeElement} className={`code-edit-code language-${language}`} dangerouslySetInnerHTML={{ __html: displayCode }} />
          </pre>
        </div>
      </div>
    </>
  );
};

export default CodeEditor;
