import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from '@mui/material';
import { FunctionComponent, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { addSourceFile, getCurrentVideo } from '../core/projectSlice';

const AddFileButton: FunctionComponent<{}> = () => {
  const files = useAppSelector(s => getCurrentVideo(s.project)?.sourceFileIds.map(id => s.project.sourceFiles[id]) || []);
  const dispatch = useAppDispatch();

  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogPath, setDialogPath] = useState('');

  const onAdd = () => {
    setDialogPath('');
    setIsDialogOpen(true);
  };

  const onSaveAdded = () => {
    setIsDialogOpen(false);
    dispatch(addSourceFile(dialogPath));
  };

  return (
    <>
      <Button onClick={onAdd}>Add File</Button>
      <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
        <DialogTitle>Add File</DialogTitle>
        <DialogContent>
          <DialogContentText>Enter the file's path as it will appear in the video.</DialogContentText>
          <TextField autoFocus fullWidth variant="standard" value={dialogPath} onChange={e => setDialogPath(e.target.value)} />
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={() => setIsDialogOpen(false)}>
            Cancel
          </Button>
          <Button color="primary" variant="contained" onClick={onSaveAdded} disabled={!dialogPath || files.some(f => f.path === dialogPath)}>
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AddFileButton;
