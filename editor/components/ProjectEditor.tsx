import { FunctionComponent, useState } from 'react';
import { Box, Button, Divider, Grid, Paper, Stack } from '@mui/material';
import VideoSetup from '../components/VideoSetup';
import RevisionList from '../components/RevisionList';
import FileSetup from '../components/FileSetup';
import RevisionEditor from './RevisionEditor';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { saveProject } from '../core/projectSlice';
import Player from './Player';

const ProjectEditor: FunctionComponent<{}> = () => {
  const [preview, setPreview] = useState(false);

  const dispatch = useAppDispatch();
  const currentRevisionId = useAppSelector(s => s.project.currentRevisionId);
  return (
    <>
      {preview && <Player onClose={() => setPreview(false)} />}
      {!preview && (
        <Box sx={{ position: 'absolute', height: '100vh', width: '100%' }}>
          <Grid container spacing={2} wrap="nowrap">
            <Grid item>
              <Paper elevation={3} square sx={{ width: 600 }}>
                <Stack sx={{ p: 2 }}>
                  <Button onClick={() => dispatch(saveProject())}>Save</Button>
                  <Divider sx={{ my: 2, mx: -2 }} />
                  <VideoSetup onPreview={() => setPreview(true)} />
                  <Divider sx={{ my: 2, mx: -2 }} />
                  <RevisionList />
                  <Divider sx={{ my: 2, mx: -2 }} />
                  <FileSetup />
                </Stack>
              </Paper>
            </Grid>
            <Grid item sx={{ width: '100%' }}>
              {currentRevisionId && <RevisionEditor />}
            </Grid>
          </Grid>
        </Box>
      )}
    </>
  );
};

export default ProjectEditor;
