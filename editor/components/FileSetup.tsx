import { Grid } from '@mui/material';
import { FunctionComponent } from 'react';
import AddFileButton from './AddFileButton';

const FileSetup: FunctionComponent<{}> = () => {
  return (
    <Grid container>
      <Grid item xs />
      <Grid item>
        <AddFileButton />
      </Grid>
      <Grid item xs />
    </Grid>
  );
};

export default FileSetup;
