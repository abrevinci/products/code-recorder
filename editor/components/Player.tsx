import { Backdrop, Box, Button, CircularProgress, Grid, Slider, styled } from '@mui/material';
import { FunctionComponent, useEffect, useRef, useState } from 'react';
import { useAppSelector } from '../core/hooks';
import { getCurrentVideo } from '../core/projectSlice';
import { CodeFrame, getCodeFrames } from '../utils/animationUtils';
import { getMergedAudioBuffer } from '../utils/audioUtils';
import util from 'audio-buffer-utils';
import Prism from 'prismjs';

enum AudioPlayerState {
  PLAYING = 'playing',
  PAUSED = 'paused',
  FINISHED = 'finished',
}

class AudioPlayer {
  private audioContext: AudioContext;
  private buffer: null | AudioBuffer;
  public startPosition: number;
  private lastPlayPosition: number;
  private sourceNode: null | AudioBufferSourceNode;
  private scriptNode: ScriptProcessorNode;
  private state: AudioPlayerState;

  public onaudioprocess: null | ((time: number) => void);
  public onplay: null | (() => void);
  public onpause: null | (() => void);
  public onfinish: null | (() => void);

  constructor(audioContext: AudioContext) {
    this.audioContext = audioContext;
    this.buffer = null;
    this.startPosition = 0;
    this.lastPlayPosition = this.audioContext.currentTime;
    this.sourceNode = null;
    this.scriptNode = this.audioContext.createScriptProcessor();
    this.scriptNode.connect(this.audioContext.destination);
    this.state = AudioPlayerState.PAUSED;

    this.onaudioprocess = null;
    this.onplay = null;
    this.onpause = null;
    this.onfinish = null;
  }

  load(buffer: AudioBuffer) {
    this.startPosition = 0;
    this.lastPlayPosition = this.audioContext.currentTime;
    this.buffer = buffer;
    this.createSource(); // is this needed here?
  }

  play(start?: number) {
    if (!this.buffer) {
      return;
    }

    this.createSource();

    if (start === undefined) {
      start = this.getCurrentTime();
      if (start >= this.getDuration()) {
        start = 0;
      }
    }
    this.startPosition = start;
    this.lastPlayPosition = this.audioContext.currentTime;

    this.sourceNode?.start(0, start);
    this.setState(AudioPlayerState.PLAYING);
    this.onplay && this.onplay();
  }

  pause() {
    this.startPosition += this.getPlayedTime();
    try {
      this.sourceNode && this.sourceNode.stop(0);
    } catch (err) {
      // ignore
    }
    this.setState(AudioPlayerState.PAUSED);
    this.onpause && this.onpause();
  }

  getCurrentTime(): number {
    switch (this.state) {
      case AudioPlayerState.PLAYING:
        return this.startPosition + this.getPlayedTime();
      case AudioPlayerState.PAUSED:
        return this.startPosition;
      case AudioPlayerState.FINISHED:
        return this.getDuration();
    }
  }

  getDuration(): number {
    return this.buffer ? this.buffer.duration : 0;
  }

  private getPlayedTime(): number {
    return this.audioContext.currentTime - this.lastPlayPosition;
  }

  private setState(state: AudioPlayerState) {
    if (this.state !== state) {
      this.state = state;
      switch (state) {
        case AudioPlayerState.PLAYING: {
          this.addOnAudioProcess();
          break;
        }
        case AudioPlayerState.PAUSED: {
          this.removeOnAudioProcess();
          break;
        }
        case AudioPlayerState.FINISHED: {
          this.removeOnAudioProcess();
          this.onfinish && this.onfinish();
          break;
        }
      }
    }
  }

  private addOnAudioProcess() {
    this.scriptNode.onaudioprocess = () => {
      const time = this.getCurrentTime();

      if (time >= this.getDuration()) {
        this.setState(AudioPlayerState.FINISHED);
        this.onpause && this.onpause();
      } else if (this.state === AudioPlayerState.PLAYING) {
        this.onaudioprocess && this.onaudioprocess(time);
      }
    };
  }

  private removeOnAudioProcess() {
    this.scriptNode.onaudioprocess = null;
  }

  private createSource() {
    this.disconnectSource();
    this.sourceNode = this.audioContext.createBufferSource();
    this.sourceNode.buffer = this.buffer;
    this.sourceNode.connect(this.audioContext.destination);
  }

  private disconnectSource() {
    if (this.sourceNode) {
      this.sourceNode.disconnect();
    }
  }
}

const audioContext = new AudioContext();

type PlayerProps = {
  onClose: () => void;
};

const StyledSlider = styled(Slider)(({ theme }) => ({
  '& .MuiSlider-mark': {
    height: 16,
    width: 2,
    '&.MuiSlider-markActive': {
      opacity: 1,
      backgroundColor: 'currentColor',
    },
  },
  '& .MuiSlider-markLabel': {
    transform: 'translateX(-2px)',
    '&.MuiSlider-markLabelActive': {
      opacity: 1,
      color: 'currentColor',
    },
  },
}));

const Player: FunctionComponent<PlayerProps> = ({ onClose }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [file, setFile] = useState(' ');
  const [language, setLanguage] = useState('csharp');
  const [duration, setDuration] = useState(0);
  const [playhead, setPlayhead] = useState(0);
  const [sliderValue, setSliderValue] = useState<number | undefined>(undefined);
  const [marks, setMarks] = useState<{ value: number; label: string }[]>([]);

  const codeElement = useRef<HTMLElement>(null);
  const fileContainerElement = useRef<HTMLElement>(null);
  const project = useAppSelector(s => s.project);
  const video = useAppSelector(s => getCurrentVideo(s.project));

  const playerRef = useRef<AudioPlayer>();
  const codeFramesRef = useRef<CodeFrame[]>();

  useEffect(() => {
    if (!video) {
      return;
    }
    const prepare = async () => {
      setIsLoaded(false);

      const revisions = video.revisionIds.map(ri => project.revisions[ri]);
      const audioClips = revisions.map(r => r.audioClipIds.map(ai => project.audioClips[ai]));
      const audioBuffers = await Promise.all<AudioBuffer>(audioClips.map(clips => getMergedAudioBuffer(clips, audioContext)));
      const codeFrames: CodeFrame[] = [];
      const fileMarks: { value: number; label: string }[] = [];
      let lastFilePath = '';
      let offset = 0;
      for (let i = 0; i < revisions.length; i += 1) {
        const revision = revisions[i];
        const file = project.sourceFiles[revision.sourceFileId];
        const fileRevisionIndex = file.revisionIds.indexOf(revision.id);
        const prevRevision = fileRevisionIndex > 0 ? project.revisions[file.revisionIds[fileRevisionIndex - 1]] : undefined;
        const duration = audioBuffers[i].duration;
        codeFrames.push(...getCodeFrames(offset, duration, file.path, revision, prevRevision));
        if (file.path !== lastFilePath) {
          lastFilePath = file.path;
          const pathComponents = lastFilePath.split('/');
          fileMarks.push({ value: offset, label: pathComponents.length > 0 ? pathComponents[pathComponents.length - 1] : '' });
        }
        offset += duration;
      }

      playerRef.current = new AudioPlayer(audioContext);
      playerRef.current.load(util.concat(audioBuffers));
      playerRef.current.onplay = () => setIsPlaying(true);
      playerRef.current.onpause = () => setIsPlaying(false);
      playerRef.current.onfinish = () => {
        setIsPlaying(false);
        makeOnAudioProcess()(0);
      };
      codeFramesRef.current = codeFrames;

      setIsLoaded(true);
      setDuration(playerRef.current.getDuration());
      setMarks(fileMarks);
      makeOnAudioProcess()(0);
    };
    prepare();
  }, [project, video]);

  const makeOnAudioProcess = () => {
    let codeFrameIndex = -1;
    return (time: number) => {
      if (codeFramesRef.current) {
        const currentCodeFrame = codeFramesRef.current.findIndex(f => f.startTime <= time && f.endTime >= time);
        if (currentCodeFrame >= 0 && currentCodeFrame != codeFrameIndex) {
          codeFrameIndex = currentCodeFrame;
          const codeFrame = codeFramesRef.current[codeFrameIndex];

          const codeToShow = codeFrame.code
            .replace(/<highlight>/g, '___HIGHLIGHT_BEGIN___')
            .replace(/<\/highlight>/g, '___HIGHLIGHT_END___')
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/___HIGHLIGHT_END___/g, '</highlight>')
            .replace(/___HIGHLIGHT_BEGIN___/g, '<highlight>')
            .replace(/\/\*___SQUIGGLY_BEGIN___\*\//g, '<squiggly>')
            .replace(/\/\*___SQUIGGLY_END___\*\//g, '</squiggly>');

          const displayCode = codeToShow.length === 0 || codeToShow[codeToShow.length - 1] === '\n' ? codeToShow + ' ' : codeToShow;

          const language = codeFrame.filePath.endsWith('.xaml') || codeFrame.filePath.endsWith('.csproj') ? 'xml' : 'csharp';

          if (codeElement.current) {
            codeElement.current.innerHTML = displayCode;
            Prism.highlightElement(codeElement.current);
          }
          setFile(codeFrame.filePath);
          setLanguage(language);
        }
        setPlayhead(time);
      }
    };
  };

  const onPlay = () => {
    if (!playerRef.current) {
      return;
    }

    playerRef.current.onaudioprocess = makeOnAudioProcess();
    playerRef.current.play(playhead);
  };

  const onPause = () => {
    if (playerRef.current) {
      playerRef.current.pause();
    }
  };

  const onSeek = () => {
    if (playerRef.current && sliderValue !== undefined) {
      if (isPlaying) {
        onPause();
        playerRef.current.startPosition = sliderValue;
        playerRef.current.onaudioprocess && playerRef.current.onaudioprocess(sliderValue);
        onPlay();
      } else {
        playerRef.current.startPosition = sliderValue;
        makeOnAudioProcess()(sliderValue);
      }
      setSliderValue(undefined);
    }
  };

  return (
    <>
      <div className="code-editor">
        <pre className={`language-${language}`} style={{ paddingLeft: '3.8em', opacity: 0.7, borderBottom: '1px solid black', marginTop: 0, marginBottom: 0 }}>
          <code className={`language-${language}`} ref={fileContainerElement}>
            <span className="token variable">{file}</span>
          </code>
        </pre>
        <div className="code-edit-container">
          <pre className={`code-edit-output language-${language} styled-scrollbars line-numbers`}>
            <code ref={codeElement} className={`code-edit-code language-${language}`} />
          </pre>
        </div>
      </div>
      <Grid container spacing={1}>
        <Grid item>
          {!isPlaying && <Button onClick={onPlay}>Play</Button>}
          {isPlaying && <Button onClick={onPause}>Pause</Button>}
        </Grid>
        <Grid item xs />
        <Grid item>
          <Button onClick={onClose} disabled={isPlaying}>
            Close
          </Button>
        </Grid>
      </Grid>
      <Box sx={{ mx: 2 }}>
        <StyledSlider
          size="small"
          min={0}
          max={duration}
          value={sliderValue !== undefined ? sliderValue : playhead}
          onChange={(_, v) => setSliderValue(v as any)}
          onChangeCommitted={() => onSeek()}
          marks={marks}
          step={0.01}
        />
      </Box>
      <Backdrop sx={{ color: '#fff', zIndex: theme => theme.zIndex.drawer + 1 }} open={!isLoaded}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};

export default Player;
