import { Circle, Stop } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Button } from '@mui/material';
import { FunctionComponent, useRef, useState } from 'react';
import { bufferToWave, loadAudioBuffer } from '../utils/audioUtils';

type AudioRecorderProps = {
  onBeganRecording: () => void;
  onFinishedRecording: (blob: Blob, duration: number) => void;
};

const AudioRecorder: FunctionComponent<AudioRecorderProps> = ({ onBeganRecording, onFinishedRecording }) => {
  const recorderRef = useRef<MediaRecorder | null>(null);
  const audioContextRef = useRef<AudioContext>();
  const [isActive, setIsActive] = useState(false);
  const [isRecording, setIsRecording] = useState(false);

  const start = async () => {
    if (!audioContextRef.current) {
      audioContextRef.current = new AudioContext();
    }
    if (recorderRef.current) {
      return;
    }
    onBeganRecording();
    setIsRecording(true);
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    recorderRef.current = new MediaRecorder(stream);
    const chunks: Blob[] = [];
    recorderRef.current.ondataavailable = e => {
      chunks.push(e.data);
    };
    recorderRef.current.onstart = () => {
      setIsActive(true);
    };
    recorderRef.current.onstop = e => {
      if (chunks.length > 0) {
        const audioData = new Blob(chunks, { type: chunks[0].type });
        setIsActive(false);
        setIsRecording(false);
        loadAudioBuffer(audioData, audioContextRef.current!).then(buffer => {
          const blob = bufferToWave(buffer, 0, buffer.length);
          const duration = buffer.duration;
          onFinishedRecording(blob, duration);
        });
      }
    };
    recorderRef.current.start();
  };

  const stop = async () => {
    if (!recorderRef.current) {
      return;
    }
    recorderRef.current.stop();
    recorderRef.current.stream.getAudioTracks().forEach(track => track.stop());
    recorderRef.current = null;
  };

  return (
    <>
      {isActive && (
        <Button variant="outlined" startIcon={<Stop />} onClick={stop}>
          Stop
        </Button>
      )}
      {!isActive && (
        <LoadingButton loading={isRecording && !isActive} variant="outlined" startIcon={!isActive && <Circle sx={{ fill: 'red' }} />} onClick={start}>
          Record
        </LoadingButton>
      )}
    </>
  );
};

export default AudioRecorder;
