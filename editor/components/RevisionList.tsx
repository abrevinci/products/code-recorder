import { Accordion, AccordionDetails, AccordionSummary, Stack, Typography } from '@mui/material';
import { FunctionComponent, useMemo } from 'react';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import RevisionView from './RevisionView';
import { setRevisionsExpanded } from '../core/projectSlice';

const RevisionList: FunctionComponent<{}> = () => {
  const dispatch = useAppDispatch();
  const revisionIdsAndFilePaths: [number, string][] = useAppSelector(s =>
    s.project.currentVideoId ? s.project.videos[s.project.currentVideoId].revisionIds.map(id => [id, s.project.sourceFiles[s.project.revisions[id].sourceFileId].path]) : []
  );
  const expandedRevisionIds: number[] = useAppSelector(s => (s.project.currentVideoId ? s.project.videos[s.project.currentVideoId].expandedRevisionIds : []));

  const revisionGroups = useMemo(() => {
    const groups: { filePath: string; ids: { id: number; prevId?: number; nextId?: number }[] }[] = [];
    let index = 0;
    for (const [id, filePath] of revisionIdsAndFilePaths) {
      const ids: { id: number; prevId?: number; nextId?: number } = { id };
      if (index > 0) {
        ids.prevId = revisionIdsAndFilePaths[index - 1][0];
      }
      if (index < revisionIdsAndFilePaths.length - 1) {
        ids.nextId = revisionIdsAndFilePaths[index + 1][0];
      }
      if (groups.length > 0 && groups[groups.length - 1].filePath === filePath) {
        groups[groups.length - 1].ids.push(ids);
      } else {
        groups.push({ filePath, ids: [ids] });
      }
      index += 1;
    }
    return groups;
  }, [revisionIdsAndFilePaths]);

  return (
    <>
      <Typography variant="h5" sx={{ mb: 2 }}>
        Source File Revisions
      </Typography>
      <div>
        {revisionGroups.map(({ filePath, ids }, index) => (
          <Accordion
            key={index}
            expanded={ids.some(id => expandedRevisionIds.includes(id.id))}
            onChange={(_, e) => dispatch(setRevisionsExpanded({ ids: ids.map(id => id.id), expanded: e }))}
            elevation={3}
            sx={{ backgroundColor: theme => theme.palette.grey[200] }}
          >
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography fontWeight="bold">{filePath}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Stack spacing={1}>
                {ids.map(props => {
                  return <RevisionView key={props.id} {...props} />;
                })}
              </Stack>
            </AccordionDetails>
          </Accordion>
        ))}
      </div>
    </>
  );
};

export default RevisionList;
