import { Slider, styled } from '@mui/material';
import { FunctionComponent, useEffect, useState } from 'react';

const StyledSlider = styled(Slider)({
  '& .MuiSlider-thumb': {
    zIndex: 4,
  },
});

type AnimationRangeSliderProps = {
  value: [number, number];
  setValue: (value: [number, number]) => void;
  disabled: boolean;
  max: number;
};

const AnimationRangeSlider: FunctionComponent<AnimationRangeSliderProps> = ({ value, setValue, disabled, max }) => {
  const [state, setState] = useState(value);
  useEffect(() => {
    setState(value);
  }, [value]);

  return (
    <StyledSlider
      disabled={disabled}
      size="small"
      value={state}
      onChange={(_, v) => setState(v as any)}
      onChangeCommitted={() => setValue(state)}
      max={max}
      step={0.01}
      valueLabelDisplay="auto"
    />
  );
};

export default AnimationRangeSlider;
