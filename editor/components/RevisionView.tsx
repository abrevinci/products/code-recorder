import { AddCircle, ArrowDownward, ArrowUpward, Delete, PlayCircle, StopCircle } from '@mui/icons-material';
import { Badge, Grid, IconButton, Paper, styled, Theme, Typography } from '@mui/material';
import { FunctionComponent, useRef, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { addRevision, removeRevision, setCurrentRevisionId, swapRevisions } from '../core/projectSlice';
import { urlToAudioBuffer } from '../utils/audioUtils';
import util from 'audio-buffer-utils';

type RevisionViewProps = {
  id: number;
  prevId?: number;
  nextId?: number;
};

const StyledPaper = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(1),
  userSelect: 'none',
  width: '100%',
}));

const RevisionView: FunctionComponent<RevisionViewProps> = ({ id, prevId, nextId }) => {
  const dispatch = useAppDispatch();
  const [revision] = useAppSelector(s => [s.project.revisions[id], s.project.sourceFiles[s.project.revisions[id].sourceFileId]]);
  const audioClips = useAppSelector(s => s.project.revisions[id].audioClipIds.map(i => s.project.audioClips[i]));
  const selected = useAppSelector(s => s.project.currentRevisionId === id);
  const [isPlaying, setIsPlaying] = useState(false);

  const sourceRef = useRef<AudioBufferSourceNode | null>();

  const onPlayClicked = async (e: any) => {
    e.stopPropagation();
    setIsPlaying(true);
    const ac = new AudioContext();
    const buffers: AudioBuffer[] = [];
    for (const audioClip of audioClips) {
      const buffer = await urlToAudioBuffer(audioClip.url, ac);
      const trimmedBuffer = util.slice(buffer, buffer.sampleRate * audioClip.start, Math.min(buffer.sampleRate * audioClip.end, buffer.length - 1));
      buffers.push(trimmedBuffer);
    }
    sourceRef.current = ac.createBufferSource();
    sourceRef.current.buffer = util.concat(buffers);
    sourceRef.current.connect(ac.destination);
    sourceRef.current.onended = () => {
      if (sourceRef.current) {
        sourceRef.current.disconnect();
        sourceRef.current = null;
      }
      setIsPlaying(false);
    };
    sourceRef.current.start();
  };

  const onStopClicked = (e: any) => {
    e.stopPropagation();
    if (sourceRef.current) {
      sourceRef.current.stop();
    }
  };

  const onUpClicked = (e: any) => {
    e.stopPropagation();
    if (prevId) {
      dispatch(swapRevisions({ firstId: prevId, secondId: id }));
    }
  };

  const onDownClicked = (e: any) => {
    e.stopPropagation();
    if (nextId) {
      dispatch(swapRevisions({ firstId: id, secondId: nextId }));
    }
  };

  const onViewClicked = (e: any) => {
    e.stopPropagation();
    if (!selected) {
      dispatch(setCurrentRevisionId(id));
    }
  };

  const onDeleteClicked = (e: any) => {
    e.stopPropagation();
    dispatch(removeRevision(id));
  };

  const onAddClicked = (e: any) => {
    e.stopPropagation();
    dispatch(addRevision(id));
  };

  return (
    <>
      <Badge badgeContent={revision.fileAdded ? '+' : '-'} color={revision.fileAdded ? 'success' : 'error'} invisible={!revision.fileAdded && !revision.fileRemoved}>
        <StyledPaper
          elevation={3}
          sx={selected ? { backgroundColor: theme => theme.palette.primary.main, color: theme => theme.palette.primary.contrastText } : {}}
          onClick={onViewClicked}
        >
          <Grid container wrap="nowrap" spacing={2} alignItems="center">
            <Grid item xs zeroMinWidth>
              <Typography
                variant="subtitle2"
                sx={{
                  fontWeight: 400,
                  opacity: 0.6,
                  wordBreak: 'break-word',
                  overflow: 'hidden',
                  lineHeight: '1em',
                  maxHeight: '3em',
                  textOverflow: 'ellipsis',
                  display: '-webkit-box',
                  WebkitLineClamp: 3,
                  lineClamp: 3,
                  WebkitBoxOrient: 'vertical',
                }}
              >
                {revision.transcript}
              </Typography>
            </Grid>
            <Grid item>
              {!isPlaying && (
                <IconButton onClick={onPlayClicked} sx={audioClips.length === 0 ? { opacity: 0 } : {}} disabled={audioClips.length === 0}>
                  <PlayCircle sx={selected ? { fill: (theme: Theme) => theme.palette.primary.contrastText } : {}} />
                </IconButton>
              )}
              {isPlaying && (
                <IconButton onClick={onStopClicked} sx={audioClips.length === 0 ? { opacity: 0 } : {}} disabled={audioClips.length === 0}>
                  <StopCircle sx={selected ? { fill: (theme: Theme) => theme.palette.primary.contrastText } : {}} />
                </IconButton>
              )}
              <IconButton onClick={onAddClicked}>
                <AddCircle sx={selected ? { fill: (theme: Theme) => theme.palette.primary.contrastText } : {}} />
              </IconButton>
              <IconButton onClick={onDeleteClicked}>
                <Delete sx={selected ? { fill: (theme: Theme) => theme.palette.primary.contrastText } : {}} />
              </IconButton>
              <IconButton onClick={onUpClicked} sx={!prevId ? { opacity: 0 } : {}} disabled={!prevId}>
                <ArrowUpward sx={selected ? { fill: (theme: Theme) => theme.palette.primary.contrastText } : {}} />
              </IconButton>
              <IconButton onClick={onDownClicked} sx={!nextId ? { opacity: 0 } : {}} disabled={!nextId}>
                <ArrowDownward sx={selected ? { fill: (theme: Theme) => theme.palette.primary.contrastText } : {}} />
              </IconButton>
            </Grid>
          </Grid>
        </StyledPaper>
      </Badge>
    </>
  );
};

export default RevisionView;
