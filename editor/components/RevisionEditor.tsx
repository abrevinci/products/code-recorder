import { Box, Grid, Stack, Tab, Tabs } from '@mui/material';
import { FunctionComponent, useCallback, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { addRevision, getCurrentRevision, onAudioClipRecorded, onAudioClipRecording, setCode, setRevisionEditorTab, setTranscript } from '../core/projectSlice';
import AudioRecorder from './AudioRecorder';
import CodeEditor from './CodeEditor';
import AudioClipView from './AudioClipView';
import TranscriptEditor from './TranscriptEditor';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import RevisionAnimationController from './RevisionAnimationController';

const RevisionEditor: FunctionComponent<{}> = () => {
  const [previewCode, setPreviewCode] = useState<string | null>(null);

  const dispatch = useAppDispatch();
  const tab = useAppSelector(s => s.project.revisionEditorTab);
  const revision = useAppSelector(s => getCurrentRevision(s.project));
  const sourceFiles = useAppSelector(s => s.project.sourceFiles);

  const clipIds = revision?.audioClipIds || [];
  const code = revision?.code || '';
  const filePath = revision?.sourceFileId ? sourceFiles[revision.sourceFileId].path : '';
  const transcript = revision?.transcript || '';

  const setTab = (value: string) => {
    dispatch(setRevisionEditorTab(value));
  };

  const onSetTranscript = (value: string) => {
    dispatch(setTranscript(value));
  };

  const onCreateRevision = useCallback(() => {
    if (revision?.id) {
      dispatch(addRevision(revision.id));
    }
  }, [revision?.id]);

  return (
    <>
      <Box sx={{ typography: 'body1', my: 2, mr: 2 }}>
        <TabContext value={tab}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={(_, v) => setTab(v)}>
              <Tab label="Code" value="1" />
              <Tab label="Audio" value="2" />
            </TabList>
          </Box>
          <TabPanel value="1" sx={{ px: 0, pb: 0 }}>
            <Stack spacing={1}>
              <Box sx={{ width: '1173px' }}>
                <TranscriptEditor value={transcript} setValue={onSetTranscript} />
              </Box>
              <Box sx={{ width: '1173px' }}>
                <RevisionAnimationController setPreviewCode={setPreviewCode} />
              </Box>
              <Box>
                <CodeEditor
                  preview={!!previewCode}
                  code={previewCode ? previewCode : code}
                  filePath={filePath}
                  setCode={c => dispatch(setCode(c))}
                  createRevision={onCreateRevision}
                />
              </Box>
            </Stack>
          </TabPanel>
          <TabPanel value="2" sx={{ px: 0, pb: 0 }}>
            <Stack spacing={1}>
              <TranscriptEditor value={transcript} setValue={onSetTranscript} />
              {clipIds.map((id, index) => {
                const prevId = index > 0 ? clipIds[index - 1] : undefined;
                const nextId = index < clipIds.length - 1 ? clipIds[index + 1] : undefined;
                return <AudioClipView key={id} id={id} prevId={prevId} nextId={nextId} />;
              })}
              <AudioRecorder
                onBeganRecording={() => dispatch(onAudioClipRecording())}
                onFinishedRecording={(blob, duration) => dispatch(onAudioClipRecorded({ blob, duration }))}
              />
            </Stack>
          </TabPanel>
        </TabContext>
      </Box>
    </>
  );
};

export default RevisionEditor;
