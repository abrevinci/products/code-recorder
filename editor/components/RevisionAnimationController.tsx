import { FunctionComponent, useCallback, useRef, useState } from 'react';
import { getMergedAudioBuffer, urlToAudioBuffer } from '../utils/audioUtils';
import WaveSurferContainer, { WaveSurferRef } from '../ws/containers/WaveSurferContainer';
import util from 'audio-buffer-utils';
import { getCurrentRevision, getPreviousRevision, setAnimationRange, setHighlightChanges, setUseTypeWriter } from '../core/projectSlice';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import WaveForm from '../ws/components/WaveForm';
import AnimationRangeSlider from './AnimationRangeSlider';
import { Button, Checkbox, FormControlLabel, Grid } from '@mui/material';
import { getDiffRevisions } from '../utils/diffUtils';
import { CodeFrame } from '../utils/animationUtils';

const plugins: any[] = [];

type RevisionAnimationControllerProps = {
  setPreviewCode: (previewCode: string | null) => void;
};

const RevisionAnimationController: FunctionComponent<RevisionAnimationControllerProps> = ({ setPreviewCode }) => {
  const [isPreviewing, setIsPreviewing] = useState(false);

  const dispatch = useAppDispatch();
  const clipIds = useAppSelector(s => getCurrentRevision(s.project)?.audioClipIds || []);
  const audioClips = useAppSelector(s => s.project.audioClips);
  const revision = useAppSelector(s => getCurrentRevision(s.project));
  const files = useAppSelector(s => s.project.sourceFiles);
  const prevRevision = useAppSelector(s => getPreviousRevision(s.project));

  const totalDuration = clipIds.map(id => audioClips[id]).reduce((acc, clip) => acc + clip.end - clip.start, 0);
  const animationRange = revision?.animationRange || [0, 0];
  const code = revision?.code || '';
  const previousCode = revision && prevRevision && revision.sourceFileId === prevRevision.sourceFileId ? prevRevision.code : '';
  const useTypeWriter = revision ? revision.useTypeWriter : false;
  const highlightChanges = revision ? revision.highlightChanges : false;
  const filePath = revision ? files[revision.sourceFileId].path : '';

  const wavesurferRef = useRef<WaveSurferRef>();
  const codeFramesRef = useRef<CodeFrame[]>();

  const onWaveSurferMount = useCallback(
    (waveSurfer: WaveSurferRef) => {
      const loadBuffers = async () => {
        if (wavesurferRef.current) {
          wavesurferRef.current.on('finish', () => {
            codeFramesRef.current = undefined;
            setIsPreviewing(false);
          });
          wavesurferRef.current.on('pause', () => {
            codeFramesRef.current = undefined;
            setIsPreviewing(false);
          });
          wavesurferRef.current.on('audioprocess', time => {
            if (codeFramesRef.current) {
              const currentCodeFrame = codeFramesRef.current.find(f => f.startTime <= time && f.endTime >= time);
              if (currentCodeFrame && !currentCodeFrame.passed) {
                currentCodeFrame.passed = true;
                setPreviewCode(currentCodeFrame.code);
              } else if (!currentCodeFrame) {
                setPreviewCode(null);
              }
            }
          });

          const clips = clipIds.map(id => audioClips[id]);
          const compositeBuffer = await getMergedAudioBuffer(clips, new AudioContext());
          wavesurferRef.current.loadDecodedBuffer(compositeBuffer);
        }
      };
      wavesurferRef.current = waveSurfer;
      loadBuffers().then(() => {
        if (window) {
          (window as any).surferidze = wavesurferRef.current;
        }
      });
    },
    [revision, clipIds, audioClips, animationRange, previousCode, setPreviewCode]
  );

  const onPreview = () => {
    if (!wavesurferRef.current) {
      return;
    }
    if (previousCode) {
      const revisions = getDiffRevisions(previousCode, code, useTypeWriter, highlightChanges);
      if (revisions.length === 0) {
        return;
      }
      const frames: CodeFrame[] = [];
      frames.push({ startTime: 0, endTime: animationRange[0], filePath, code: previousCode, passed: false });
      const step = (animationRange[1] - animationRange[0]) / revisions.length;
      let time = animationRange[0];
      for (const revision of revisions) {
        frames.push({ startTime: time, endTime: time + step, filePath, code: revision, passed: false });
        time += step;
      }
      frames.push({ startTime: animationRange[1], endTime: totalDuration, filePath, code, passed: false });

      codeFramesRef.current = frames;
    }
    setIsPreviewing(true);
    wavesurferRef.current.play();
  };

  return (
    <Grid container spacing={1} alignItems="center">
      {previousCode && (
        <Grid item>
          <div>
            <FormControlLabel
              control={<Checkbox checked={highlightChanges} onChange={e => dispatch(setHighlightChanges(e.target.checked))} disabled={isPreviewing} />}
              label="Highlight Changes"
            />
          </div>
          <div>
            <FormControlLabel
              control={<Checkbox checked={useTypeWriter} onChange={e => dispatch(setUseTypeWriter(e.target.checked))} disabled={isPreviewing} />}
              label="Use Typewriter"
            />
          </div>
        </Grid>
      )}
      <Grid item xs>
        {clipIds.length > 0 && (
          <div>
            <WaveSurferContainer plugins={plugins} onMount={onWaveSurferMount}>
              <WaveForm id={`composite-waveform-${revision?.id}`} splitChannels hideScrollbar normalize responsive height={80}></WaveForm>
            </WaveSurferContainer>
            {previousCode && <AnimationRangeSlider value={animationRange} setValue={v => dispatch(setAnimationRange(v))} disabled={isPreviewing} max={totalDuration} />}
          </div>
        )}
      </Grid>
      <Grid item>
        {clipIds.length > 0 && (
          <Button onClick={onPreview} variant="contained" disabled={isPreviewing}>
            Preview
          </Button>
        )}
      </Grid>
    </Grid>
  );
};

export default RevisionAnimationController;
