import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, TextField } from '@mui/material';
import { FunctionComponent, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { addFollowUpVideo, addNewVideo, getCurrentVideo } from '../core/projectSlice';

const AddVideoButton: FunctionComponent<{}> = () => {
  const videos = useAppSelector(s => s.project.videoIds.map(id => s.project.videos[id]));
  const currentVideo = useAppSelector(s => getCurrentVideo(s.project));
  const dispatch = useAppDispatch();

  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogName, setDialogName] = useState('');
  const [dialogContinue, setDialogContinue] = useState(false);

  const onAdd = () => {
    setDialogName('');
    setDialogContinue(videos.length > 0);
    setIsDialogOpen(true);
  };

  const onSaveAdded = () => {
    setIsDialogOpen(false);
    if (dialogContinue) {
      dispatch(addFollowUpVideo(dialogName));
    } else {
      dispatch(addNewVideo(dialogName));
    }
  };

  return (
    <>
      <Button onClick={onAdd}>Add Video</Button>
      <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
        <DialogTitle>Add Video</DialogTitle>
        <DialogContent>
          <DialogContentText>Choose a name for the video.</DialogContentText>
          <TextField autoFocus fullWidth variant="standard" value={dialogName} onChange={e => setDialogName(e.target.value)} />
          {videos.length > 0 && (
            <FormControlLabel
              control={<Checkbox checked={dialogContinue} onChange={e => setDialogContinue(e.target.checked)} />}
              label={`Continue from "${currentVideo?.name}"`}
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={() => setIsDialogOpen(false)}>
            Cancel
          </Button>
          <Button color="primary" variant="contained" onClick={onSaveAdded} disabled={!dialogName || videos.some(v => v.name === dialogName)}>
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AddVideoButton;
