import { TextField } from '@mui/material';
import { FunctionComponent, useEffect, useState } from 'react';

type TranscriptEditorProps = {
  value: string;
  setValue: (value: string) => void;
};

const TranscriptEditor: FunctionComponent<TranscriptEditorProps> = ({ value, setValue }) => {
  const [state, setState] = useState(value);

  useEffect(() => {
    setState(value);
  }, [value]);

  return (
    <TextField name="transcript" sx={{ width: '100%' }} label="Transcript" multiline value={state} onChange={e => setState(e.target.value)} onBlur={() => setValue(state)} />
  );
};

export default TranscriptEditor;
