import { Button, FormControl, Grid, InputLabel, MenuItem, Select } from '@mui/material';
import { FunctionComponent } from 'react';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { getCurrentVideo, setCurrentVideoId } from '../core/projectSlice';
import AddVideoButton from './AddVideoButton';

type VideoSetupProps = {
  onPreview: () => void;
};

const VideoSetup: FunctionComponent<VideoSetupProps> = ({ onPreview }) => {
  const videos = useAppSelector(s => s.project.videoIds.map(id => s.project.videos[id]));
  const currentVideo = useAppSelector(s => getCurrentVideo(s.project));
  const dispatch = useAppDispatch();

  const onSelectVideo = (id: number) => {
    if (id) {
      dispatch(setCurrentVideoId(id));
    }
  };

  return (
    <Grid container spacing={1} alignItems="center">
      {videos.length > 0 && (
        <Grid item>
          <FormControl fullWidth>
            <InputLabel id="videoSelect">Video</InputLabel>
            <Select labelId="videoSelect" value={currentVideo?.id || ''} label="Video" onChange={e => onSelectVideo(+e.target.value)}>
              {videos.map(v => (
                <MenuItem key={v.id} value={v.id}>
                  {v.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      )}
      <Grid item>
        <AddVideoButton />
      </Grid>
      <Grid item xs />
      <Grid item>
        <Button onClick={onPreview}>Preview</Button>
      </Grid>
    </Grid>
  );
};

export default VideoSetup;
