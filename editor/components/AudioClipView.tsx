import { ArrowUpward, ArrowDownward, Delete, PlayCircle, ContentCut, StopCircle } from '@mui/icons-material';
import { Grid, IconButton, Paper, styled } from '@mui/material';
import { FunctionComponent, useCallback, useEffect, useRef, useState } from 'react';
import RegionsPlugin from 'wavesurfer.js/src/plugin/regions';
import WebAudio from 'wavesurfer.js/src/webaudio';
import { useAppDispatch, useAppSelector } from '../core/hooks';
import { onAudioClipUpdated, removeAudioClip, setAudioClipRange, swapAudioClips } from '../core/projectSlice';
import { bufferToWave, getTrimmedAudio } from '../utils/audioUtils';
import Region from '../ws/components/Region';
import WaveForm from '../ws/components/WaveForm';
import WaveSurferContainer, { WaveSurferRef } from '../ws/containers/WaveSurferContainer';

const plugins: any[] = [
  {
    plugin: RegionsPlugin,
    options: { dragSelection: false },
  },
];

type WaveformEditorProps = {
  id: number;
  prevId?: number;
  nextId?: number;
};

const StyledPaper = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(1),
  userSelect: 'none',
}));

const AudioClipView: FunctionComponent<WaveformEditorProps> = ({ id, prevId, nextId }) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const dispatch = useAppDispatch();
  const clip = useAppSelector(s => s.project.audioClips[id]);

  const wavesurferRef = useRef<WaveSurferRef>();
  const onWaveSurferMount = useCallback(
    (waveSurfer: WaveSurferRef) => {
      wavesurferRef.current = waveSurfer;
      if (wavesurferRef.current) {
        wavesurferRef.current.on('finish', () => setIsPlaying(false));
        wavesurferRef.current.on('pause', () => setIsPlaying(false));
        wavesurferRef.current.on('play', () => setIsPlaying(true));
        if (clip?.url) {
          wavesurferRef.current.load(clip.url);
        }
        if (window) {
          (window as any).surferidze = wavesurferRef.current;
        }
      }
    },
    [clip.url]
  );

  const onPlay = () => {
    if (wavesurferRef.current) {
      wavesurferRef.current.play(clip.start, clip.end);
    }
  };

  const onStop = () => {
    if (wavesurferRef.current) {
      wavesurferRef.current.pause();
    }
  };

  const onDelete = () => {
    dispatch(removeAudioClip(id));
  };

  const onTrim = () => {
    if (wavesurferRef.current) {
      const buffer = getTrimmedAudio(clip.start, clip.end, (wavesurferRef.current.backend as any).buffer, (wavesurferRef.current.backend as WebAudio).ac);
      const blob = bufferToWave(buffer, 0, buffer.length);
      dispatch(onAudioClipUpdated({ id, url: clip.url, blob, duration: buffer.duration }));
    }
  };

  useEffect(() => {
    if (clip?.url && wavesurferRef.current) {
      wavesurferRef.current.load(clip.url);
    }
  }, [clip.url]);

  const onRegionUpdate = (region: any) => {
    dispatch(setAudioClipRange({ id, start: region.start, end: region.end }));
    if (wavesurferRef.current) {
      if (wavesurferRef.current.getCurrentTime() < region.start) {
        wavesurferRef.current.setCurrentTime(region.start);
      } else if (wavesurferRef.current.getCurrentTime() > region.end) {
        wavesurferRef.current.setCurrentTime(region.end);
      }
    }
  };

  const onUpClicked = (e: any) => {
    e.stopPropagation();
    if (prevId) {
      dispatch(swapAudioClips({ firstId: id, secondId: prevId }));
    }
  };

  const onDownClicked = (e: any) => {
    e.stopPropagation();
    if (nextId) {
      dispatch(swapAudioClips({ firstId: id, secondId: nextId }));
    }
  };

  return (
    <>
      <StyledPaper elevation={3}>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs>
            <WaveSurferContainer plugins={plugins} onMount={onWaveSurferMount}>
              <WaveForm id={`waveform-${id}`} splitChannels hideScrollbar normalize responsive height={80}>
                <Region id={`selection-${id}`} color="rgba(0, 0, 255, .05)" start={clip.start} end={clip.end} onUpdateEnd={onRegionUpdate} />
              </WaveForm>
            </WaveSurferContainer>
          </Grid>
          <Grid item>
            {!isPlaying && (
              <IconButton onClick={onPlay}>
                <PlayCircle />
              </IconButton>
            )}
            {isPlaying && (
              <IconButton onClick={onStop}>
                <StopCircle />
              </IconButton>
            )}
            <IconButton onClick={onTrim}>
              <ContentCut />
            </IconButton>
            <IconButton onClick={onDelete}>
              <Delete />
            </IconButton>
            <IconButton onClick={onUpClicked} sx={!prevId ? { opacity: 0 } : {}} disabled={!prevId}>
              <ArrowUpward />
            </IconButton>
            <IconButton onClick={onDownClicked} sx={!nextId ? { opacity: 0 } : {}} disabled={!nextId}>
              <ArrowDownward />
            </IconButton>
          </Grid>
        </Grid>
      </StyledPaper>
    </>
  );
};

export default AudioClipView;
