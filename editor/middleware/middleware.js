import nextConnect from 'next-connect';
import multiparty from 'multiparty';

const middleware = nextConnect();

middleware.use(async (req, _, next) => {
  const form = new multiparty.Form();

  await form.parse(req, (_, fields, files) => {
    req.body = fields;
    req.files = files;
    next();
  });
});

export default middleware;
