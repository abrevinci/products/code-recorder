import { AudioClip } from '../core/projectSlice';
import util from 'audio-buffer-utils';

export const getTrimmedAudio = (start: number, end: number, originalBuffer: AudioBuffer, audioContext: AudioContext) => {
  const segmentDuration = end - start;

  var emptySegment = audioContext.createBuffer(originalBuffer.numberOfChannels, segmentDuration * originalBuffer.sampleRate + 1, originalBuffer.sampleRate);
  for (var i = 0; i < originalBuffer.numberOfChannels; i++) {
    var chanData = originalBuffer.getChannelData(i);
    var emptySegmentData = emptySegment.getChannelData(i);
    var mid_data = chanData.subarray(start * originalBuffer.sampleRate, end * originalBuffer.sampleRate);
    emptySegmentData.set(mid_data);
  }

  return emptySegment;
};

export const bufferToWave = (abuffer: AudioBuffer, offset: number, len: number) => {
  var numOfChan = abuffer.numberOfChannels,
    length = len * numOfChan * 2 + 44,
    buffer = new ArrayBuffer(length),
    view = new DataView(buffer),
    channels = [],
    i,
    sample,
    pos = 0;

  // write WAVE header
  setUint32(0x46464952); // "RIFF"
  setUint32(length - 8); // file length - 8
  setUint32(0x45564157); // "WAVE"

  setUint32(0x20746d66); // "fmt " chunk
  setUint32(16); // length = 16
  setUint16(1); // PCM (uncompressed)
  setUint16(numOfChan);
  setUint32(abuffer.sampleRate);
  setUint32(abuffer.sampleRate * 2 * numOfChan); // avg. bytes/sec
  setUint16(numOfChan * 2); // block-align
  setUint16(16); // 16-bit (hardcoded in this demo)

  setUint32(0x61746164); // "data" - chunk
  setUint32(length - pos - 4); // chunk length

  // write interleaved data
  for (i = 0; i < abuffer.numberOfChannels; i++) channels.push(abuffer.getChannelData(i));

  while (pos < length) {
    for (i = 0; i < numOfChan; i++) {
      // interleave channels
      sample = Math.max(-1, Math.min(1, channels[i][offset])); // clamp
      sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767) | 0; // scale to 16-bit signed int
      view.setInt16(pos, sample, true); // update data chunk
      pos += 2;
    }
    offset++; // next source sample
  }

  // create Blob
  return new Blob([buffer], { type: 'audio/wav' });

  function setUint16(data: number) {
    view.setUint16(pos, data, true);
    pos += 2;
  }

  function setUint32(data: number) {
    view.setUint32(pos, data, true);
    pos += 4;
  }
};

export const loadAudioBuffer = (blob: Blob, audioContext: AudioContext) => {
  return new Promise<AudioBuffer>(resolve => {
    const fileReader = new FileReader();
    fileReader.onloadend = () => {
      resolve(audioContext.decodeAudioData(fileReader.result as ArrayBuffer));
    };
    fileReader.readAsArrayBuffer(blob);
  });
};

export const urlToAudioBuffer = async (url: string, audioContext: AudioContext) => {
  const arrayBuffer = await fetch(url).then(r => r.arrayBuffer());
  return await audioContext.decodeAudioData(arrayBuffer);
};

export const getMergedAudioBuffer = async (clips: AudioClip[], audioContext: AudioContext) => {
  const buffers: AudioBuffer[] = [];
  for (const clip of clips) {
    const buffer = await urlToAudioBuffer(clip.url, audioContext);
    const trimmedBuffer = util.slice(buffer, buffer.sampleRate * clip.start, Math.min(buffer.sampleRate * clip.end, buffer.length - 1));
    buffers.push(trimmedBuffer);
  }
  return util.concat(buffers);
};
