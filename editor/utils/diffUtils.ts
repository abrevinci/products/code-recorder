import Diff from 'text-diff';

export const createDiff = (text1: string, text2: string): [number, string][] => {
  const diff = new Diff();
  const textDiff = diff.main(text1, text2);
  diff.cleanupSemantic(textDiff);
  return textDiff;
};

export const getDiffRevisions = (text1: string, text2: string, typeWriter: boolean, highlight: boolean): string[] => {
  const wrapInHighlightIfRequested = (text: string) => (highlight ? `<highlight>${text}</highlight>` : text);
  const diff = createDiff(text1, text2);
  let revisions: string[] = [];
  for (let i = 0; i < diff.length; i += 1) {
    const before = diff
      .filter((d, idx) => idx < i && d[0] !== -1)
      .map(d => d[1])
      .join('');
    const after = diff
      .filter((d, idx) => idx > i && d[0] !== 1)
      .map(d => d[1])
      .join('');
    if (diff[i][0] === -1) {
      const removed = diff[i][1];
      revisions.push(before + wrapInHighlightIfRequested(removed) + after);
      revisions.push(before + wrapInHighlightIfRequested('') + after);
    } else if (diff[i][0] === 1) {
      const added = diff[i][1];
      if (typeWriter) {
        for (let a = 1; a < added.length; a += 1) {
          revisions.push(before + wrapInHighlightIfRequested(added.substring(0, a)) + after);
        }
      } else {
        revisions.push(before + wrapInHighlightIfRequested(added) + after);
      }
    }
  }
  return revisions;
};
