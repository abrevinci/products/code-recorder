import { Revision, SourceFile } from '../core/projectSlice';
import { getDiffRevisions } from './diffUtils';

export type CodeFrame = {
  startTime: number;
  endTime: number;
  filePath: string;
  code: string;
  passed: boolean;
};

export const getCodeFrames = (offset: number, duration: number, filePath: string, revision: Revision, prevRevision?: Revision) => {
  const frames: CodeFrame[] = [];
  if (prevRevision) {
    const codes = getDiffRevisions(prevRevision.code, revision.code, revision.useTypeWriter, revision.highlightChanges);
    frames.push({ startTime: offset, endTime: offset + revision.animationRange[0], filePath, code: prevRevision.code, passed: false });
    const step = (revision.animationRange[1] - revision.animationRange[0]) / codes.length;
    let time = offset + revision.animationRange[0];
    for (const code of codes) {
      frames.push({ startTime: time, endTime: time + step, filePath, code, passed: false });
      time += step;
    }
    frames.push({ startTime: offset + revision.animationRange[1], endTime: offset + duration, filePath, code: revision.code, passed: false });
  } else {
    frames.push({ startTime: offset, endTime: offset + duration, filePath, code: revision.code, passed: false });
  }
  return frames;
};
