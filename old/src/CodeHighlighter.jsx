function CodeHighlighter({ codeHtml, scale = 4 }) {
  return (
    <html lang="en">
      <body
        style={{
          width: `${1920 / scale}px`,
          height: `${1080 / scale}px`,
          background: '#272822',
          transform: `scale(${scale})`,
          transformOrigin: '0% 0% 0px',
          margin: 0,
        }}
      >
        <div
          style={{
            display: 'flex',
            margin: '20px 0 0 2px',
          }}
        >
          <pre
            style={{
              margin: 0,
            }}
          >
            <code
              style={{
                fontFamily: "Consolas,Monaco,'Andale Mono','Ubuntu Mono',monospace",
              }}
              dangerouslySetInnerHTML={{
                __html: codeHtml,
              }}
            />
          </pre>
        </div>
      </body>
    </html>
  );
}

export default CodeHighlighter;
