import { renderToStaticMarkup } from 'react-dom/server';
import { JSDOM } from 'jsdom';
import { readFileSync } from 'fs';

// Prism.js
import Prism from 'prismjs';
import loadLanguages from 'prismjs/components/';
import 'prismjs/components/prism-csharp';
import 'prismjs/plugins/line-numbers/prism-line-numbers';
// import 'prismjs/plugins/line-numbers/prism-line-numbers.css';
// import 'prismjs/themes/prism-okaidia.css';

// My custom React component
import CodeHighlighter from './CodeHighlighter.jsx';

const styling = readFileSync('./node_modules/prismjs/themes/prism-okaidia.css', { encoding: 'utf8' });

export const generateHtml = (code, language) => {
  loadLanguages([language]);
  const codeHtml = Prism.highlight(code, Prism.languages[language], language);

  // get HTML string
  const html = renderToStaticMarkup(<CodeHighlighter codeHtml={codeHtml} scale={2} />);

  const { window } = new JSDOM(html);
  const { document } = window;

  // Add Prism.js styling to the HTML document
  const style = document.createElement('style');
  style.textContent = styling;
  document.head.appendChild(style);

  return document.getElementsByTagName('html')[0].outerHTML;
};
