import Prism from 'prismjs';
import { FunctionComponent, useEffect, useRef } from 'react';
import 'prismjs/components/prism-csharp';
import 'prismjs/plugins/line-numbers/prism-line-numbers';
import 'prismjs/plugins/line-numbers/prism-line-numbers.css';
import 'prismjs/themes/prism-okaidia.css';

type PrismCodeProps = {
  code: string;
  language: string;
  plugins?: string[];
};

const PrismCode: FunctionComponent<PrismCodeProps> = ({ code, language = 'csharp', plugins = ['line-numbers'] }) => {
  const codeElement = useRef(null);

  const highlight = () => {
    if (codeElement.current) {
      Prism.highlightElement(codeElement.current);
    }
  };

  useEffect(() => {
    highlight();
  });

  return (
    <div>
      <pre className={plugins.join(' ')}>
        <code ref={codeElement} className={`language-${language}`}>
          {code.trim()}
        </code>
      </pre>
    </div>
  );
};

export default PrismCode;
