import React from 'react';
import ReactDOM from 'react-dom';
import PrismCode from './PrismCode';

const defaultCode = `
public record State();
`.trim();

ReactDOM.render(
  <React.StrictMode>
    <PrismCode code={defaultCode} language="csharp" />
  </React.StrictMode>,
  document.getElementById('root')
);
