import puppeteer from 'puppeteer';
import { renderToStaticMarkup } from 'react-dom/server';
import { PuppeteerScreenRecorder } from 'puppeteer-screen-recorder';
import PrismCode from './PrismCode';
import { readFileSync } from 'fs';
import Diff from 'text-diff';

import Prism from 'prismjs';
// import 'prismjs/components/prism-csharp';
// import 'prismjs/plugins/line-numbers/prism-line-numbers';
// import 'prismjs/plugins/line-numbers/prism-line-numbers.css';
// import 'prismjs/themes/prism-okaidia.css';
import { generateHtml } from './utils';

const prismCss = readFileSync('./node_modules/prismjs/themes/prism-okaidia.css', { encoding: 'utf8' });
const lineNumbersCss = readFileSync('./node_modules/prismjs/plugins/line-numbers/prism-line-numbers.css', { encoding: 'utf8' });

const generateVideo = async (textDiff, changes, language) => {
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--window-size=1920,1080'],
    defaultViewport: {
      width: 1920,
      height: 1080,
    },
  });
  const page = await browser.newPage();
  const config = {
    followNewTab: false,
    fps: 25,
    ffmpeg_Path: null,
    videoFrame: {
      width: 1920,
      height: 1080,
    },
    aspectRatio: '16:9',
  };

  const recorder = new PuppeteerScreenRecorder(page, config);

  await recorder.start('./output.mp4');

  const code = textDiff
    .filter(diff => diff[0] != 1)
    .map(diff => diff[1])
    .join('');
  const html = generateHtml(code, language);

  await page.setContent(html);

  await page.waitForTimeout(1000);

  // for (const change of changes) {
  //   const modifiedCode = textDiff
  //     .filter()
  // }

  await recorder.stop();
  await browser.close();
};

const codeToRecord = `
namespace MyApp.Core;

public abstract record Message
{
  // todo: add messages here
}
`;

const codeAfter = `
namespace MyApp.Core;

public abstract record Message
{
  public sealed record Increment : Message;
}
`;

const diff = new Diff();
const textDiff = diff.main(codeToRecord, codeAfter);
diff.cleanupSemantic(textDiff);

console.log(textDiff);

const changes = [[1, 2]];

const generateCode = (d, c, i) => {
  if (i === 0 || c.length === 0) {
    return d
      .filter(diff => diff[0] != 1)
      .map(diff => diff[1])
      .join('');
  }

  const appliedChanges = c.slice(0, i).flat();
  return d
    .filter((diff, index) => diff[0] != 1 || appliedChanges.find(v => v == index))
    .map(diff => (diff[0] >= 0 ? diff[1] : ''))
    .join('');
};

describe('generateCode', () => {
  it('should generate correct initial code', () => {
    const code = generateCode(textDiff, changes, 0);
    expect(code).toBe(codeToRecord);
  });
  it('should generate correct changed code', () => {
    const code = generateCode(textDiff, changes, 1);
    expect(code).toBe(codeAfter);
  });
});

// describe('App.js', () => {
//   console.log(textDiff);

//   it('should render video', async () => {
//     await generateVideo(textDiff, changes, 'cs');
//   });
// });
